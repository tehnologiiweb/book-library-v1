﻿using BookLibrary.Core.SeedWork;

namespace BookLibrary.Core.DataModel
{
    public class Rentals : Entity
    {
        public Rentals(int bookId, int readerId, DateTime rentalStartDate, DateTime expectedRentalEndDate, DateTime? actualRentalEndDate = null)
        {
            BookId = bookId;
            ReaderId = readerId;
            RentalStartDate = rentalStartDate;
            ExpectedRentalEndDate = expectedRentalEndDate;
            ActualRentalEndDate = actualRentalEndDate;
        }

        public int BookId { get; set; }
        public int ReaderId { get; set; }
        public DateTime RentalStartDate { get; set; }
        public DateTime ExpectedRentalEndDate { get; set; }
        public DateTime? ActualRentalEndDate { get; set; }
        public virtual Users Reader { get; set; } = null!;
    }
}
