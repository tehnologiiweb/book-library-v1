﻿using BookLibrary.Core.SeedWork;

namespace BookLibrary.Core.DataModel
{
    public class Books : Entity, IAggregateRoot
    {
        public Books(string name, string author, string genre, int maximumDaysForRental, bool isBooked = false)
        {
            Name = name;
            Author = author;
            Genre = genre;
            MaximumDaysForRental = maximumDaysForRental;
            IsBooked = isBooked;
        }

        public string Name { get; set; }
        public string Author { get; set; }
        public string Genre { get; set; }
        public int MaximumDaysForRental { get; set; }
        public bool IsBooked { get; set; }
        public virtual ICollection<Keywords> Keywords { get; set; } = new List<Keywords>();
    }
}