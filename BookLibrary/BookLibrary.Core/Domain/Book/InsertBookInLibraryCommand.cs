﻿using BookLibrary.Core.SeedWork;

namespace BookLibrary.Core.Domain.Book
{
    public record InsertBookInLibraryCommand : ICreateAggregateCommand
    {
        public string Name { get; init; }
        public string Author { get; init; }
        public string Genre { get; init; }
        public int MaximumDaysForRental { get; init; }
        public IEnumerable<KeyWordDto> Keywords { get; init; } = new List<KeyWordDto>();

        public InsertBookInLibraryCommand(string name, string author, string genre, int maximumDaysForRental, IEnumerable<KeyWordDto> keywords)
        {
            Name = name;
            Author = author;
            Genre = genre;
            MaximumDaysForRental = maximumDaysForRental;
            Keywords = keywords;
        }
    }

    public record KeyWordDto (string Name, string Description);
}
