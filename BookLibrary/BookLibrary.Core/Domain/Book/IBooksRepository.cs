﻿using BookLibrary.Core.DataModel;
using BookLibrary.Core.SeedWork;

namespace BookLibrary.Core.Domain.Book
{
    public interface IBooksRepository : IRepositoryOfAggregate<Books, InsertBookInLibraryCommand>
    {
        public Task DeleteBookAsync(int bookId, CancellationToken cancellationToken);
    }
}
