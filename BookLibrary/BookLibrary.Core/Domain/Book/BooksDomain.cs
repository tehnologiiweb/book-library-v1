﻿using BookLibrary.Core.DataModel;
using BookLibrary.Core.SeedWork;

namespace BookLibrary.Core.Domain.Book
{
    public class BooksDomain : DomainOfAggregate<Books>
    {
        public BooksDomain(Books aggregate) : base(aggregate)
        {
        }

        public void UpdateBook(string name, string author, string genre, int maximumDaysForRental, ICollection<Keywords> keywords)
        {
            aggregate.Name = name;
            aggregate.Author = author;
            aggregate.Genre = genre;
            aggregate.MaximumDaysForRental = maximumDaysForRental;
            aggregate.Keywords = keywords;
        }

        public bool BookCanBeRented(int rentalDays) => !aggregate.IsBooked && rentalDays <= aggregate.MaximumDaysForRental;
        public void MarkBookAsRented() => aggregate.IsBooked = true;
        public void MarkBookAsAvailable() => aggregate.IsBooked = false;
        public bool BookCanBeDeleted() => !aggregate.IsBooked;
    }
}
