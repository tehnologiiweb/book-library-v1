﻿using BookLibrary.Core.DataModel;
using BookLibrary.Core.SeedWork;

namespace BookLibrary.Core.Domain.UserRentals
{
    public class UsersRentalsDomain : DomainOfAggregate<Users>
    {
        public UsersRentalsDomain(Users aggregate) : base(aggregate)
        {
        }

        public void UpdateProfile(string name, string email, string phone, string address)
        {
            aggregate.Name = name;
            aggregate.Email = email;
            aggregate.PhoneNumber = phone;
            aggregate.Address = address;
        }

        public UserRentedBookEvent RentBook(int bookId, int rentalDays)
        {
            if (aggregate.Rentals.Where(x => !x.ActualRentalEndDate.HasValue).Count() > 3)
            {
                throw new TooManyRentalsException();
            }

            aggregate.Rentals.Add(new Rentals(bookId, aggregate.Id, DateTime.Now, DateTime.Now.AddDays(rentalDays)));

            return new UserRentedBookEvent(bookId);
        }

        public UserReturnedBookEvent ReturnBook(int bookId)
        {
            var rental = aggregate.Rentals
                .FirstOrDefault(x => x.BookId == bookId && !x.ActualRentalEndDate.HasValue);

            if (rental == null)
            {
                throw new RentalNotFoundException(bookId);
            }

            rental.ActualRentalEndDate = DateTime.Now;
            
            return new UserReturnedBookEvent(bookId);
        }

        public int GetProfileId() => aggregate.Id;
    }
}
