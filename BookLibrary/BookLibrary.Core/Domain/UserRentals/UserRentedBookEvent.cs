﻿using MediatR;

namespace BookLibrary.Core.Domain.UserRentals
{
    public record UserRentedBookEvent : INotification
    {
        public int BookId { get; private set; }
        public UserRentedBookEvent(int bookId)
        {
            BookId = bookId;
        }
    }
}
