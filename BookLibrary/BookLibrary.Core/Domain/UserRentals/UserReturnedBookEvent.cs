﻿using MediatR;

namespace BookLibrary.Core.Domain.UserRentals
{
    public record UserReturnedBookEvent : INotification
    {
        public int BookId { get; private set; }
        public UserReturnedBookEvent(int bookId)
        {
            BookId = bookId;   
        }
    }
}
