﻿namespace BookLibrary.Core.Domain.UserRentals
{
    public class RentalNotFoundException : Exception
    {
        public RentalNotFoundException(int bookId) : base($"Rental for book {bookId} either finished or not found!")
        {
        }
    }
}
