﻿namespace BookLibrary.Core.Domain.UserRentals
{
    public class TooManyRentalsException : Exception
    {
        public TooManyRentalsException() : base("Current user has too many rentals at this moment!")
        {
        }
    }
}
