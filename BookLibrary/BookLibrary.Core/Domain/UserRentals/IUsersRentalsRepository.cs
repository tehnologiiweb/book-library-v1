﻿using BookLibrary.Core.DataModel;
using BookLibrary.Core.SeedWork;

namespace BookLibrary.Core.Domain.UserRentals
{
    public interface IUsersRentalsRepository : IRepositoryOfAggregate<Users, RegisterUserProfileCommand>
    {
        public Task<DomainOfAggregate<Users>?> GetByIdentityAsync (string identityId, CancellationToken cancellationToken);
        public Task<DomainOfAggregate<Users>?> GetWithoutRentalsAsync(int id, CancellationToken cancellationToken);
        public Task<DomainOfAggregate<Users>?> GetByIdentityWithoutRentalsAsync(string identityId, CancellationToken cancellationToken);

    }
}
