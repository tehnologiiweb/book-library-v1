﻿using BookLibrary.Core.Domain.UserRentals;
using BookLibrary.Core.Domain.Book;
using BookLibrary.Infrastructure.Data;
using BookLibrary.Infrastructure.Data.Repositories;
using Microsoft.EntityFrameworkCore;

namespace BookLibrary.Api.Infrastructure
{
    public static partial class DataAccessExtensions
    {
        public static void AddBookLibraryDbContext(this WebApplicationBuilder builder)
        {
            builder.Services.AddDbContext<BookLibraryContext>(opt =>
                opt.UseSqlServer(builder.Configuration.GetConnectionString("BookLibraryDb")));
        }

        public static void AddBookLibraryAggregateRepositories(this IServiceCollection services)
        {
            services.AddTransient<IBooksRepository, BooksRepository>();
            services.AddTransient<IUsersRentalsRepository, UsersRentalsRepository>();
        }
    }
}
