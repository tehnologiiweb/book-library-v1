﻿using BookLibrary.Api.Features.Books;
using BookLibrary.Api.Features.Metrics;
using BookLibrary.Api.Features.Profile;
using BookLibrary.Api.Features.Rentals;

namespace BookLibrary.Api.Web
{
    public static class ApiFeaturesExtensions
    {
        public static void AddApiFeaturesHandlers(this IServiceCollection services)
        {
            // Add Book Handlers
            services.AddBooksHandlers();

            // Add Profile Handlers
            services.AddProfilesHandlers();

            // Add Rentals Handlers
            services.AddRentalsHandlers();

            // Add Metrics Handlers
            services.AddMetricsHandlers();
        }
    }
}
