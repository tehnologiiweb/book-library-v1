﻿using BookLibrary.Api.Features.Metrics.ViewMetricsAboutBook;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace BookLibrary.Api.Features.Metrics
{
    [Route("api/v1/[controller]")]
    [ApiController]
    public class MetricsController : ControllerBase
    {
        private readonly IViewMetricsAboutBookQueryHandler viewMetricsAboutBookQueryHandler;

        public MetricsController(IViewMetricsAboutBookQueryHandler viewMetricsAboutBookQueryHandler)
        {
            this.viewMetricsAboutBookQueryHandler = viewMetricsAboutBookQueryHandler;
        }

        [HttpGet("metricsAboutBook/{id}")]
        [Authorize("AdminAccess")]
        public async Task<ActionResult<ViewMetricsAboutBookDto>> ViewMetricsAboutBookAsync([FromRoute] int id, CancellationToken cancellationToken)
        {
            var metric = await viewMetricsAboutBookQueryHandler.HandleAsync(id, cancellationToken);

            return metric;
        }
    }
}
