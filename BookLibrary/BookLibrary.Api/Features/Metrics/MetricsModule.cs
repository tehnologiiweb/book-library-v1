﻿using BookLibrary.Api.Features.Metrics.ViewMetricsAboutBook;

namespace BookLibrary.Api.Features.Metrics
{
    internal static class MetricsModule
    {
        internal static void AddMetricsHandlers(this IServiceCollection services)
        {
            services.AddTransient<IViewMetricsAboutBookQueryHandler, ViewMetricsAboutBookQueryHandler>();
        }
    }
}
