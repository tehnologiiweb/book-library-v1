﻿using BookLibrary.Api.Web;
using BookLibrary.Infrastructure.Data;
using Microsoft.EntityFrameworkCore;

namespace BookLibrary.Api.Features.Metrics.ViewMetricsAboutBook
{
    public class ViewMetricsAboutBookQueryHandler : IViewMetricsAboutBookQueryHandler
    {
        private readonly BookLibraryContext dbContext;

        public ViewMetricsAboutBookQueryHandler(BookLibraryContext dbContext)
        {
            this.dbContext = dbContext;
        }

        public async Task<ViewMetricsAboutBookDto> HandleAsync(int bookId, CancellationToken cancellationToken)
        {
            var query = from book in dbContext.Books
                        where book.Id == bookId
                        join rental in dbContext.Rentals.Include(x => x.Reader)
                            on book.Id equals rental.BookId into grouping
                        from rental in grouping.DefaultIfEmpty()
                        select new BookRentalQueryDto(book, rental);

            var result = await query
                .AsNoTracking()
                .ToListAsync(cancellationToken);

            var metric = new ViewMetricsAboutBookDto(bookId, result[0].BookName, result[0].AuthorName);

            foreach (var bookRental in result)
            {
                var rentalDto = bookRental.ValidateAndParseRental();

                if (rentalDto != null)
                {
                    metric.AddRentalToHistory(rentalDto);
                }
            }

            metric.DivideAverages();

            return metric;
        }
    }
}
