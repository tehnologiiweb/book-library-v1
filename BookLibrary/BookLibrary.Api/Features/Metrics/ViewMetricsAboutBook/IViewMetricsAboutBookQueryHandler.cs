﻿namespace BookLibrary.Api.Features.Metrics.ViewMetricsAboutBook
{
    public interface IViewMetricsAboutBookQueryHandler
    {
        public Task<ViewMetricsAboutBookDto> HandleAsync(int bookId, CancellationToken cancellationToken);
    }
}
