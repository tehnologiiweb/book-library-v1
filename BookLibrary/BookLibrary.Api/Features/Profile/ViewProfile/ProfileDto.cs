﻿using BookLibrary.Api.Features.Profile.RegisterProfile;

namespace BookLibrary.Api.Features.Profile.ViewProfile
{
    public record ProfileDto : RegisterProfileCommand
    {
        public ProfileDto(string email, string name, string phoneNumber, string address) : base(email, name, phoneNumber, address)
        {
        }
    }
}
