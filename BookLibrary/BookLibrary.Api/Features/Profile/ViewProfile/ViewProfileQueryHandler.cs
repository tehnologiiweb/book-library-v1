﻿using BookLibrary.Api.Web;
using BookLibrary.Infrastructure.Data;
using Microsoft.EntityFrameworkCore;

namespace BookLibrary.Api.Features.Profile.ViewProfile
{
    public class ViewProfileQueryHandler : IViewProfileQueryHandler
    {
        private readonly BookLibraryContext dbContext;

        public ViewProfileQueryHandler(BookLibraryContext dbContext)
        {
            this.dbContext = dbContext;
        }

        public async Task<ProfileDto> HandleAsync(string identityId, CancellationToken cancellationToken)
        {
            var userProfile = await dbContext.Users
                .Where(x => x.IdentityId == identityId)
                .Select(x => new ProfileDto(x.Email, x.Name, x.PhoneNumber, x.Address))
                .FirstOrDefaultAsync(cancellationToken);

            if (userProfile == null)
            {
                throw new ApiException(System.Net.HttpStatusCode.Unauthorized, "This user does not have a registered profile!");
            }

            return userProfile;
        }
    }
}
