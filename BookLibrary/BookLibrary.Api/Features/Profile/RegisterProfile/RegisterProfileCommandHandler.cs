﻿using BookLibrary.Core.Domain.UserRentals;

namespace BookLibrary.Api.Features.Profile.RegisterProfile
{
    public class RegisterProfileCommandHandler : IRegisterProfileCommandHandler
    {
        private readonly IUsersRentalsRepository usersRentalsRepository;

        public RegisterProfileCommandHandler(IUsersRentalsRepository usersRentalsRepository)
        {
            this.usersRentalsRepository = usersRentalsRepository;
        }
        public Task HandleAsync(RegisterProfileCommand command, string identityId, CancellationToken cancellationToken) 
            => usersRentalsRepository.AddAsync(
                new RegisterUserProfileCommand(identityId, command.Email, command.Name, command.PhoneNumber, command.Address),
                cancellationToken);
    }
}
