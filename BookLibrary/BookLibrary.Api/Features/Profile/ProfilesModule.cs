﻿using BookLibrary.Api.Features.Profile.RegisterProfile;
using BookLibrary.Api.Features.Profile.ViewProfile;

namespace BookLibrary.Api.Features.Profile
{
    internal static class ProfilesModule
    {
        internal static void AddProfilesHandlers(this IServiceCollection services)
        {
            services.AddTransient<IRegisterProfileCommandHandler, RegisterProfileCommandHandler>();
            services.AddTransient<IViewProfileQueryHandler, ViewProfileQueryHandler>();
        }
    }
}
