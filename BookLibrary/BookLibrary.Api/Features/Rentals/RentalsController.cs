﻿using BookLibrary.Api.Authorization;
using BookLibrary.Api.Features.Rentals.RentBook;
using BookLibrary.Api.Features.Rentals.ReturnBook;
using BookLibrary.Api.Features.Rentals.ViewRentals;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace BookLibrary.Api.Features.Rentals
{
    [Route("api/v1/[controller]")]
    [ApiController]
    public class RentalsController : ControllerBase
    {
        private readonly IRentBookCommandHandler rentBookCommandHandler;
        private readonly IReturnBookCommandHandler returnBookCommandHandler;
        private readonly IViewRentalsQueryHandler viewRentalsQueryHandler;

        public RentalsController(IRentBookCommandHandler rentBookCommandHandler, IReturnBookCommandHandler returnBookCommandHandler, IViewRentalsQueryHandler viewRentalsQueryHandler)
        {
            this.rentBookCommandHandler = rentBookCommandHandler;
            this.returnBookCommandHandler = returnBookCommandHandler;
            this.viewRentalsQueryHandler = viewRentalsQueryHandler;
        }

        [HttpPut("rentBook")]
        [Authorize]
        public async Task<IActionResult> RentBookAsync([FromBody] RentBookCommand command, CancellationToken cancellationToken)
        {
            var identityId = User.GetUserIdentityId();

            if (identityId == null)
            {
                return Unauthorized();
            }
            
            await rentBookCommandHandler.HandleAsync(command, identityId, cancellationToken);

            return NoContent();
        }

        [HttpPut("returnBook/{id}")]
        [Authorize]
        public async Task<IActionResult> ReturnBookAsync([FromRoute] int id, CancellationToken cancellationToken)
        {
            var identityId = User.GetUserIdentityId();

            if (identityId == null)
            {
                return Unauthorized();
            }

            await returnBookCommandHandler.HandleAsync(id, identityId, cancellationToken);

            return NoContent();
        }

        [HttpGet("viewMyRentals")]
        [Authorize]
        public async Task<ActionResult<IEnumerable<RentalHistoryDto>>> ViewMyRentals(CancellationToken cancellationToken)
        {
            var identityId = User.GetUserIdentityId();

            if (identityId == null)
            {
                return Unauthorized();
            }

            var rentals = await viewRentalsQueryHandler.HandleAsync(identityId, cancellationToken);

            return Ok(rentals);
        }
    }
}
