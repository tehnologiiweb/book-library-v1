﻿using BookLibrary.Api.Web;
using BookLibrary.Core.Domain.UserRentals;
using BookLibrary.Infrastructure.Data;
using Microsoft.EntityFrameworkCore;
using System.Net;

namespace BookLibrary.Api.Features.Rentals.ViewRentals
{
    public class ViewRentalsQueryHandler : IViewRentalsQueryHandler
    {
        private readonly BookLibraryContext dbContext;
        
        public ViewRentalsQueryHandler(BookLibraryContext dbContext)
        {
            this.dbContext = dbContext;
        }

        public async Task<IEnumerable<RentalHistoryDto>> HandleAsync(string identityId, CancellationToken cancellationToken)
        {
            var user = await dbContext.Users.FirstOrDefaultAsync(x => x.IdentityId == identityId);

            if (user == null)
            {
                throw new ApiException(HttpStatusCode.Unauthorized, $"User with identity {identityId} does not have a registered profile");
            }

            var userProfileId = user.Id;

            var query = from rental in dbContext.Rentals
                            where rental.ReaderId == userProfileId
                        join book in dbContext.Books
                            on rental.BookId equals book.Id into grouping
                        from book in grouping.DefaultIfEmpty()
                        select new RentalHistoryDto(book, rental);

            var result = await query
                .AsNoTracking()
                .ToListAsync(cancellationToken);

            return result;
        }
    }
}
