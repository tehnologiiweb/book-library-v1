﻿namespace BookLibrary.Api.Features.Rentals.ViewRentals
{
    public interface IViewRentalsQueryHandler
    {
        public Task<IEnumerable<RentalHistoryDto>> HandleAsync(string identityId, CancellationToken cancellationToken);
    }
}
