﻿namespace BookLibrary.Api.Features.Rentals.ViewRentals
{
    public record RentalHistoryDto
    {
        public RentalHistoryDto(Core.DataModel.Books book, Core.DataModel.Rentals rental)
        {
            BookId = book.Id;
            BookName = book.Name;
            AuthorName = book.Author;
            IsCurrentlyBooked = !rental.ActualRentalEndDate.HasValue;
            RentalStartDate = rental.RentalStartDate;
            RentalEndDate = rental.ActualRentalEndDate;
            ExpectedRentalEndDate = rental.ExpectedRentalEndDate;
        }

        public int BookId { get; init; }
        public bool IsCurrentlyBooked { get; init; }
        public string BookName { get; init; }
        public string AuthorName { get; init; }
        public DateTime RentalStartDate { get; init; }
        public DateTime? RentalEndDate { get; init; }
        public DateTime? ExpectedRentalEndDate { get; init; }
    }
}
