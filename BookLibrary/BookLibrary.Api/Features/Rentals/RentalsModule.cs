﻿using BookLibrary.Api.Features.Rentals.RentBook;
using BookLibrary.Api.Features.Rentals.ReturnBook;
using BookLibrary.Api.Features.Rentals.ViewRentals;

namespace BookLibrary.Api.Features.Rentals
{
    internal static class RentalsModule
    {
        internal static void AddRentalsHandlers(this IServiceCollection services)
        {
            services.AddTransient<IRentBookCommandHandler, RentBookCommandHandler>();
            services.AddTransient<IReturnBookCommandHandler, ReturnBookCommandHandler>();
            services.AddTransient<IViewRentalsQueryHandler, ViewRentalsQueryHandler>();
        }
    }
}
