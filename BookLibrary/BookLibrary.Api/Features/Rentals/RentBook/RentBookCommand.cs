﻿namespace BookLibrary.Api.Features.Rentals.RentBook
{
    public record RentBookCommand
    {
        public RentBookCommand(int bookId, int rentalDays)
        {
            BookId = bookId;
            RentalDays = rentalDays;
        }

        public int BookId { get; init; }
        public int RentalDays { get; init; }
    }
}
