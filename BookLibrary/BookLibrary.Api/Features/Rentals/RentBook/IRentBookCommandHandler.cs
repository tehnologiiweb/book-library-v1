﻿namespace BookLibrary.Api.Features.Rentals.RentBook
{
    public interface IRentBookCommandHandler
    {
        public Task HandleAsync(RentBookCommand command, string identityId, CancellationToken cancellationToken);
    }
}
