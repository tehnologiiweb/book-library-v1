﻿using BookLibrary.Api.Web;
using BookLibrary.Core.Domain.Book;
using BookLibrary.Core.Domain.UserRentals;
using MediatR;
using System.Net;

namespace BookLibrary.Api.Features.Rentals.RentBook
{
    public class RentBookCommandHandler : IRentBookCommandHandler
    {
        private readonly IBooksRepository booksRepository;
        private readonly IUsersRentalsRepository usersRentalsRepository;
        private readonly IMediator mediator;

        public RentBookCommandHandler(IBooksRepository booksRepository, IUsersRentalsRepository usersRentalsRepository, IMediator mediator)
        {
            this.booksRepository = booksRepository;
            this.usersRentalsRepository = usersRentalsRepository;
            this.mediator = mediator;
        }

        public async Task HandleAsync(RentBookCommand command, string identityId, CancellationToken cancellationToken)
        {
            var book = await booksRepository.GetAsync(command.BookId, cancellationToken) as BooksDomain;

            if (book == null)
            {
                throw new ApiException(HttpStatusCode.NotFound, $"Book with id {command.BookId} not found!");
            }

            if (!book.BookCanBeRented(command.RentalDays))
            {
                throw new ApiException(HttpStatusCode.Conflict, $"Book with id {command.BookId} cannot be rented!");
            }

            var user = await usersRentalsRepository.GetByIdentityAsync(identityId, cancellationToken) as UsersRentalsDomain;

            if (user == null)
            {
                throw new ApiException(HttpStatusCode.Unauthorized, $"User with identity {identityId} does not have a registered profile");
            }

            try
            {
                var userRentedBookEvent = user.RentBook(command.BookId, command.RentalDays);
                await mediator.Publish(userRentedBookEvent, cancellationToken);

                await usersRentalsRepository.SaveAsync(cancellationToken);
            } 
            catch (TooManyRentalsException ex)
            {
                throw new ApiException(HttpStatusCode.Conflict, ex.Message);
            }
        }
    }
}
