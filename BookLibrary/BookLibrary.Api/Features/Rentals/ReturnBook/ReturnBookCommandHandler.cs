﻿using BookLibrary.Api.Web;
using BookLibrary.Core.Domain.UserRentals;
using MediatR;
using System.Net;

namespace BookLibrary.Api.Features.Rentals.ReturnBook
{
    public class ReturnBookCommandHandler : IReturnBookCommandHandler
    {
        private readonly IUsersRentalsRepository usersRentalsRepository;
        private readonly IMediator mediator;

        public ReturnBookCommandHandler(IUsersRentalsRepository usersRentalsRepository, IMediator mediator)
        {
            this.usersRentalsRepository = usersRentalsRepository;
            this.mediator = mediator;
        }

        public async Task HandleAsync(int bookId, string identityId, CancellationToken cancellationToken)
        {
            var user = await usersRentalsRepository.GetByIdentityAsync(identityId, cancellationToken) as UsersRentalsDomain;

            if (user == null)
            {
                throw new ApiException(HttpStatusCode.Unauthorized, $"User with identity {identityId} does not have a registered profile");
            }

            try
            {
                var userReturnedBookEvent = user.ReturnBook(bookId);

                await mediator.Publish(userReturnedBookEvent, cancellationToken);

                await usersRentalsRepository.SaveAsync(cancellationToken);
            } 
            catch (RentalNotFoundException ex)
            {
                throw new ApiException(HttpStatusCode.NotFound, ex.Message);
            }   
        }
    }
}
