﻿namespace BookLibrary.Api.Features.Rentals.ReturnBook
{
    public interface IReturnBookCommandHandler
    {
        public Task HandleAsync(int bookId, string identityId, CancellationToken cancellationToken);
    }
}
