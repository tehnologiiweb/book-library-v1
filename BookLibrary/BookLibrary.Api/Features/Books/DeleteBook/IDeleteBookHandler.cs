﻿namespace BookLibrary.Api.Features.Books.DeleteBook
{
    public interface IDeleteBookHandler
    {
        public Task HandleAsync(int id, CancellationToken cancellation);
    }
}
