﻿using BookLibrary.Api.Web;
using BookLibrary.Core.Domain.Book;

namespace BookLibrary.Api.Features.Books.DeleteBook
{
    public class DeleteBookHandler : IDeleteBookHandler
    {
        private readonly IBooksRepository booksRepository;

        public DeleteBookHandler(IBooksRepository booksRepository)
        {
            this.booksRepository = booksRepository;
        }

        public async Task HandleAsync(int id, CancellationToken cancellationToken)
        {
            var book = await booksRepository.GetAsync(id, cancellationToken) as BooksDomain;

            if (book == null)
            {
                throw new ApiException(System.Net.HttpStatusCode.NotFound, $"Book with id {id} was not found.");
            }

            if (book.BookCanBeDeleted())
            {
                await booksRepository.DeleteBookAsync(id, cancellationToken);
                await booksRepository.SaveAsync(cancellationToken);
            } else
            {
                throw new ApiException(System.Net.HttpStatusCode.Conflict, $"Book with id {id} cannot be deleted. It is probably booked!");
            }
        }
    }
}
