﻿using BookLibrary.Api.Web;
using BookLibrary.Core.Domain.Book;
using BookLibrary.Core.Domain.UserRentals;
using MediatR;
using System.Net;

namespace BookLibrary.Api.Features.Books.MarkBookAsReturned
{
    public class UserReturnedBookEventHandler : INotificationHandler<UserReturnedBookEvent>
    {
        private readonly IBooksRepository booksRepository;

        public UserReturnedBookEventHandler(IBooksRepository booksRepository)
        {
            this.booksRepository = booksRepository;
        }

        public async Task Handle(UserReturnedBookEvent notification, CancellationToken cancellationToken)
        {
            var book = await booksRepository.GetAsync(notification.BookId, cancellationToken) as BooksDomain;

            if (book == null)
            {
                throw new ApiException(HttpStatusCode.NotFound, $"Book with id {notification.BookId} not found!");
            }

            book.MarkBookAsAvailable();

            await booksRepository.SaveAsync(cancellationToken);
        }
    }
}
