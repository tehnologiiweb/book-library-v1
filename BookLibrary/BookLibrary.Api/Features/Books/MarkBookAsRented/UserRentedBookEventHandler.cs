﻿using BookLibrary.Api.Web;
using BookLibrary.Core.Domain.Book;
using BookLibrary.Core.Domain.UserRentals;
using MediatR;
using System.Net;

namespace BookLibrary.Api.Features.Books.MarkBookAsRented
{
    public class UserRentedBookEventHandler : INotificationHandler<UserRentedBookEvent>
    {
        private readonly IBooksRepository booksRepository;

        public UserRentedBookEventHandler(IBooksRepository booksRepository)
        {
            this.booksRepository = booksRepository;
        }

        public async Task Handle(UserRentedBookEvent notification, CancellationToken cancellationToken)
        {
            var book = await booksRepository.GetAsync(notification.BookId, cancellationToken) as BooksDomain;

            if (book == null)
            {
                throw new ApiException(HttpStatusCode.NotFound, $"Book with id {notification.BookId} not found!");
            }

            book.MarkBookAsRented();

            await booksRepository.SaveAsync(cancellationToken);
        }
    }
}
