﻿using BookLibrary.Core.Domain;

namespace BookLibrary.Api.Features.Books.AddBook
{
    public interface IAddBookCommandHandler
    {
        public Task HandleAsync(AddBookCommand command, CancellationToken cancellation);
    }
}
