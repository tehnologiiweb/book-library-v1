﻿using BookLibrary.Core.Domain.Book;

namespace BookLibrary.Api.Features.Books.AddBook
{
    public class AddBookCommandHandler : IAddBookCommandHandler
    {
        private readonly IBooksRepository booksRepository;

        public AddBookCommandHandler(IBooksRepository booksRepository)
        {
            this.booksRepository = booksRepository;
        }

        public Task HandleAsync(AddBookCommand command, CancellationToken cancellationToken)
            => booksRepository.AddAsync(
                new InsertBookInLibraryCommand(command.Name, command.Author, command.Genre, command.MaximumDaysForRental, command.Keywords), 
                cancellationToken);
    }
    
}
