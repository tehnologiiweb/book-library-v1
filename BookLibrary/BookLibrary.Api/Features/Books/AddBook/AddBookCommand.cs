﻿using BookLibrary.Core.Domain.Book;

namespace BookLibrary.Api.Features.Books.AddBook
{
    public record AddBookCommand : InsertBookInLibraryCommand
    {
        public AddBookCommand(string name, string author, string genre, int maximumDaysForRental, IEnumerable<KeyWordDto> keywords) : base(name, author, genre, maximumDaysForRental, keywords)
        {
        }
    }
}
