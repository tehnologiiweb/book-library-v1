﻿using BookLibrary.Infrastructure.Data;
using Microsoft.EntityFrameworkCore;

namespace BookLibrary.Api.Features.Books.ViewAllBooks
{
    public class ViewAllBooksQueryHandler : IViewAllBooksQueryHandler
    {
        private readonly BookLibraryContext dbContext;

        public ViewAllBooksQueryHandler(BookLibraryContext dbContext)
        {
            this.dbContext = dbContext;
        }

        public async Task<IEnumerable<BookDto>> HandleAsync(CancellationToken cancellationToken)
        {
            var books = await dbContext.Books
                .AsNoTracking()
                .Select(x => new BookDto(x.Id, x.Name, x.Author, x.IsBooked, x.Genre, x.MaximumDaysForRental, x.Keywords))
                .ToListAsync(cancellationToken);

            return books;
        }
    }
}
