﻿using BookLibrary.Core.DataModel;

namespace BookLibrary.Api.Features.Books.ViewAllBooks
{
    public record BookDto
    {
        public BookDto(int id, string name, string author, bool isBooked, string genre, int maximumDaysForRental, IEnumerable<Keywords> keywords)
        {
            Id = id;
            Name = name;
            Author = author;
            IsBooked = isBooked;
            Genre = genre;
            MaximumDaysForRental = maximumDaysForRental;
            Keywords = keywords;
        }

        public int Id { get; init; }
        public string Name { get; init; }
        public string Author { get; init; }
        public bool IsBooked { get; init; }
        public string Genre { get; init; }
        public int MaximumDaysForRental { get; init; }
        public IEnumerable<Keywords> Keywords { get; init; } = new List<Keywords>();
    }
}
