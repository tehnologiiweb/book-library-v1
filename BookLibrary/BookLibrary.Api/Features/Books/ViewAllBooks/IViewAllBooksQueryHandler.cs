﻿namespace BookLibrary.Api.Features.Books.ViewAllBooks
{
    public interface IViewAllBooksQueryHandler
    {
        public Task<IEnumerable<BookDto>> HandleAsync(CancellationToken cancellationToken);
    }
}
