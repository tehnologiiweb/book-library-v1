﻿using BookLibrary.Api.Features.Books.AddBook;
using BookLibrary.Api.Features.Books.DeleteBook;
using BookLibrary.Api.Features.Books.ViewAllBooks;
using BookLibrary.Api.Features.Books.ViewStatusAboutBook;

namespace BookLibrary.Api.Features.Books
{
    internal static class BooksModule
    {
        internal static void AddBooksHandlers(this IServiceCollection services)
        {
            services.AddTransient<IAddBookCommandHandler, AddBookCommandHandler>();
            services.AddTransient<IViewAllBooksQueryHandler, ViewAllBooksQueryHandler>();
            services.AddTransient<IViewStatusAboutBookQueryHandler, ViewStatusAboutBookQueryHandler>();
            services.AddTransient<IDeleteBookHandler, DeleteBookHandler>();
        }
    }
}
