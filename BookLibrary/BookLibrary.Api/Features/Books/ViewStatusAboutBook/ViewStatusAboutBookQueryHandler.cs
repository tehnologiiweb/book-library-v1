﻿using BookLibrary.Api.Web;
using BookLibrary.Infrastructure.Data;
using Microsoft.EntityFrameworkCore;

namespace BookLibrary.Api.Features.Books.ViewStatusAboutBook
{
    public class ViewStatusAboutBookQueryHandler : IViewStatusAboutBookQueryHandler
    {
        private readonly BookLibraryContext dbContext;

        public ViewStatusAboutBookQueryHandler(BookLibraryContext bookLibraryContext)
        {
            dbContext = bookLibraryContext;
        }

        public async Task<BookWithRentalStatusDto> HandleAsync(int bookId, CancellationToken cancellationToken)
        {
            var book = await dbContext.Books
                .AsNoTracking()
                .SingleOrDefaultAsync(x => x.Id == bookId, cancellationToken);

            if (book == null)
            {
                throw new ApiException(System.Net.HttpStatusCode.NotFound, $"Book with id {bookId} was not found.");
            }

            var bookWithStatus = new BookWithRentalStatusDto(book.Id, book.Name, book.Author, book.IsBooked, book.Genre, book.MaximumDaysForRental, book.Keywords);

            if (book.IsBooked)
            {

                var rentalOfBook = await dbContext
                    .Rentals
                    .AsNoTracking()
                    .Include(x => x.Reader)
                    .SingleOrDefaultAsync(x => x.BookId == bookId && x.ActualRentalEndDate == null, cancellationToken);

                if (rentalOfBook == null)
                {
                    throw new ApiException(System.Net.HttpStatusCode.Conflict, $"Book with id {bookId} appears to be booked, but no rental was found. Contact the administrator of the platform!");
                }

                bookWithStatus.ExpectedRentalEndDate = rentalOfBook.ExpectedRentalEndDate;
                bookWithStatus.ReaderEmail = rentalOfBook.Reader.Name;
                bookWithStatus.ReaderPhone = rentalOfBook.Reader.PhoneNumber;
                bookWithStatus.RentalDate = rentalOfBook.RentalStartDate;
            }

            return bookWithStatus;
        }
    }
}
