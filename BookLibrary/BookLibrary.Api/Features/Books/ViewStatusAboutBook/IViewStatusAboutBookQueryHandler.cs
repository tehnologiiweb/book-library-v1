﻿namespace BookLibrary.Api.Features.Books.ViewStatusAboutBook
{
    public interface IViewStatusAboutBookQueryHandler
    {
        public Task<BookWithRentalStatusDto> HandleAsync(int bookId, CancellationToken cancellationToken);
    }
}
