﻿using BookLibrary.Api.Features.Books.ViewAllBooks;
using BookLibrary.Core.DataModel;

namespace BookLibrary.Api.Features.Books.ViewStatusAboutBook
{
    public record BookWithRentalStatusDto : BookDto
    {
        public BookWithRentalStatusDto(int id, string name, string author, bool isBooked, string genre, int maximumDaysForRental, IEnumerable<Keywords> keywords) 
            : base(id, name, author, isBooked, genre, maximumDaysForRental, keywords)
        {
        }

        public DateTime? RentalDate { get; set; }
        public string? ReaderEmail { get; set; }
        public string? ReaderPhone { get; set; }
        public DateTime? ExpectedRentalEndDate { get; set; }
    }
}
