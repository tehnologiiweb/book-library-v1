﻿using BookLibrary.Api.Features.Books.AddBook;
using BookLibrary.Api.Features.Books.DeleteBook;
using BookLibrary.Api.Features.Books.ViewAllBooks;
using BookLibrary.Api.Features.Books.ViewStatusAboutBook;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System.Net;

namespace BookLibrary.Api.Features.Books
{
    [ApiController]
    [Route("api/v1/[controller]")]
    public class BooksController : ControllerBase
    {
        private readonly IAddBookCommandHandler addBookCommandHandler;
        private readonly IViewAllBooksQueryHandler viewAllBooksQueryHandler;
        private readonly IViewStatusAboutBookQueryHandler viewStatusAboutBookQueryHandler;
        private readonly IDeleteBookHandler deleteBookHandler;

        public BooksController(
            IAddBookCommandHandler addBookCommandHandler,
            IViewAllBooksQueryHandler viewAllBooksQueryHandler,
            IViewStatusAboutBookQueryHandler viewStatusAboutBookQueryHandler,
            IDeleteBookHandler deleteBookHandler)
        {
            this.addBookCommandHandler = addBookCommandHandler;
            this.viewAllBooksQueryHandler = viewAllBooksQueryHandler;
            this.viewStatusAboutBookQueryHandler = viewStatusAboutBookQueryHandler;
            this.deleteBookHandler = deleteBookHandler;
        }

        [HttpPost("addBook")]
        [Authorize(Policy = "AdminAccess")]
        public async Task<IActionResult> AddBookAsync([FromBody] AddBookCommand command, CancellationToken cancellationToken)
        {
            await addBookCommandHandler.HandleAsync(command, cancellationToken);

            return StatusCode((int)HttpStatusCode.Created);
        }

        [HttpGet("viewAllBooks")]
        [Authorize]
        public async Task<ActionResult<IEnumerable<BookDto>>> ViewAllBooksAsync(CancellationToken cancellationToken)
        {
            var books = await viewAllBooksQueryHandler.HandleAsync(cancellationToken);

            return Ok(books);
        }

        [HttpGet("viewStatusAboutBook/{id}")]
        [Authorize("AdminAccess")]
        public async Task<ActionResult<BookWithRentalStatusDto>> ViewStatusAboutBookAsync([FromRoute] int id, CancellationToken cancellationToken)
        {
            var book = await viewStatusAboutBookQueryHandler.HandleAsync(id, cancellationToken);

            return Ok(book);
        }

        [HttpDelete("deleteBook/{id}")]
        [Authorize("AdminAccess")]
        public async Task<IActionResult> DeleteBookAsync([FromRoute] int id, CancellationToken cancellationToken)
        {
            await deleteBookHandler.HandleAsync(id, cancellationToken);

            return NoContent();
        }
    }
}
