﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace BookLibrary.Infrastructure.Migrations
{
    public partial class RestructuredForSimplicity : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Keywords_books_BooksId",
                schema: "Library",
                table: "Keywords");

            migrationBuilder.DropForeignKey(
                name: "FK_rentals_books_BookId",
                schema: "Library",
                table: "rentals");

            migrationBuilder.DropTable(
                name: "readers",
                schema: "Library");

            migrationBuilder.DropPrimaryKey(
                name: "PK_users",
                schema: "Profile",
                table: "users");

            migrationBuilder.DropPrimaryKey(
                name: "PK_rentals",
                schema: "Library",
                table: "rentals");

            migrationBuilder.DropIndex(
                name: "IX_rentals_BookId",
                schema: "Library",
                table: "rentals");

            migrationBuilder.DropPrimaryKey(
                name: "PK_books",
                schema: "Library",
                table: "books");

            migrationBuilder.RenameTable(
                name: "users",
                schema: "Profile",
                newName: "Users");

            migrationBuilder.RenameTable(
                name: "rentals",
                schema: "Library",
                newName: "Rentals");

            migrationBuilder.RenameTable(
                name: "Keywords",
                schema: "Library",
                newName: "Keywords");

            migrationBuilder.RenameTable(
                name: "books",
                schema: "Library",
                newName: "Books");

            migrationBuilder.RenameColumn(
                name: "UserId",
                table: "Rentals",
                newName: "ReaderId");

            migrationBuilder.AlterColumn<DateTime>(
                name: "ActualRentalEndDate",
                table: "Rentals",
                type: "datetime2",
                nullable: true,
                oldClrType: typeof(DateTime),
                oldType: "datetime2");

            migrationBuilder.AddColumn<bool>(
                name: "IsBooked",
                table: "Books",
                type: "bit",
                nullable: false,
                defaultValue: false);

            migrationBuilder.AddPrimaryKey(
                name: "PK_Users",
                table: "Users",
                column: "Id");

            migrationBuilder.AddPrimaryKey(
                name: "PK_Rentals",
                table: "Rentals",
                column: "Id");

            migrationBuilder.AddPrimaryKey(
                name: "PK_Books",
                table: "Books",
                column: "Id");

            migrationBuilder.CreateIndex(
                name: "IX_Rentals_ReaderId",
                table: "Rentals",
                column: "ReaderId");

            migrationBuilder.AddForeignKey(
                name: "FK_Keywords_Books_BooksId",
                table: "Keywords",
                column: "BooksId",
                principalTable: "Books",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_Rentals_Users_ReaderId",
                table: "Rentals",
                column: "ReaderId",
                principalTable: "Users",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Keywords_Books_BooksId",
                table: "Keywords");

            migrationBuilder.DropForeignKey(
                name: "FK_Rentals_Users_ReaderId",
                table: "Rentals");

            migrationBuilder.DropPrimaryKey(
                name: "PK_Users",
                table: "Users");

            migrationBuilder.DropPrimaryKey(
                name: "PK_Rentals",
                table: "Rentals");

            migrationBuilder.DropIndex(
                name: "IX_Rentals_ReaderId",
                table: "Rentals");

            migrationBuilder.DropPrimaryKey(
                name: "PK_Books",
                table: "Books");

            migrationBuilder.DropColumn(
                name: "IsBooked",
                table: "Books");

            migrationBuilder.EnsureSchema(
                name: "Library");

            migrationBuilder.EnsureSchema(
                name: "Profile");

            migrationBuilder.RenameTable(
                name: "Users",
                newName: "users",
                newSchema: "Profile");

            migrationBuilder.RenameTable(
                name: "Rentals",
                newName: "rentals",
                newSchema: "Library");

            migrationBuilder.RenameTable(
                name: "Keywords",
                newName: "Keywords",
                newSchema: "Library");

            migrationBuilder.RenameTable(
                name: "Books",
                newName: "books",
                newSchema: "Library");

            migrationBuilder.RenameColumn(
                name: "ReaderId",
                schema: "Library",
                table: "rentals",
                newName: "UserId");

            migrationBuilder.AlterColumn<DateTime>(
                name: "ActualRentalEndDate",
                schema: "Library",
                table: "rentals",
                type: "datetime2",
                nullable: false,
                defaultValue: new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified),
                oldClrType: typeof(DateTime),
                oldType: "datetime2",
                oldNullable: true);

            migrationBuilder.AddPrimaryKey(
                name: "PK_users",
                schema: "Profile",
                table: "users",
                column: "Id");

            migrationBuilder.AddPrimaryKey(
                name: "PK_rentals",
                schema: "Library",
                table: "rentals",
                column: "Id");

            migrationBuilder.AddPrimaryKey(
                name: "PK_books",
                schema: "Library",
                table: "books",
                column: "Id");

            migrationBuilder.CreateTable(
                name: "readers",
                schema: "Library",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    CreatedAt = table.Column<DateTime>(type: "datetime2", nullable: false),
                    Email = table.Column<string>(type: "nvarchar(max)", nullable: false),
                    Phone = table.Column<string>(type: "nvarchar(max)", nullable: false),
                    UpdatedAt = table.Column<DateTime>(type: "datetime2", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_readers", x => x.Id);
                });

            migrationBuilder.CreateIndex(
                name: "IX_rentals_BookId",
                schema: "Library",
                table: "rentals",
                column: "BookId");

            migrationBuilder.AddForeignKey(
                name: "FK_Keywords_books_BooksId",
                schema: "Library",
                table: "Keywords",
                column: "BooksId",
                principalSchema: "Library",
                principalTable: "books",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_rentals_books_BookId",
                schema: "Library",
                table: "rentals",
                column: "BookId",
                principalSchema: "Library",
                principalTable: "books",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
        }
    }
}
