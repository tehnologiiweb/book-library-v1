﻿using BookLibrary.Core.DataModel;
using BookLibrary.Infrastructure.Data.EntityConfigurations;
using Microsoft.EntityFrameworkCore;

namespace BookLibrary.Infrastructure.Data
{
    public class BookLibraryContext : DbContext
    {
        public BookLibraryContext(DbContextOptions<BookLibraryContext> options) : base(options) 
        {
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.ApplyConfigurationsFromAssembly(typeof(BooksConfiguration).Assembly);
        }

        public DbSet<Users> Users => Set<Users>();
        public DbSet<Books> Books => Set<Books>();
        public DbSet<Rentals> Rentals => Set<Rentals>();
    }
}
