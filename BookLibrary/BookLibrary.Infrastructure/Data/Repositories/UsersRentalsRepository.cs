﻿using BookLibrary.Core.DataModel;
using BookLibrary.Core.Domain;
using BookLibrary.Core.Domain.UserRentals;
using BookLibrary.Core.SeedWork;
using Microsoft.EntityFrameworkCore;

namespace BookLibrary.Infrastructure.Data.Repositories
{
    public class UsersRentalsRepository : IUsersRentalsRepository
    {
        private readonly BookLibraryContext context;

        public UsersRentalsRepository(BookLibraryContext context)
        {
            this.context = context;
        }

        public async Task AddAsync(RegisterUserProfileCommand command, CancellationToken cancellationToken)
        {
            var user = new Users(command.IdentityId, command.Email, command.Name, command.PhoneNumber, command.Address);

            await context.Users.AddAsync(user);
            await SaveAsync(cancellationToken);
        }

        public async Task<DomainOfAggregate<Users>?> GetAsync(int aggregateId, CancellationToken cancellationToken)
        {
            var entity = await context.Users
                .Include(x => x.Rentals)
                .FirstOrDefaultAsync(x => x.Id == aggregateId, cancellationToken);

            if (entity == null)
            {
                return null;
            }

            return new UsersRentalsDomain(entity);
        }

        public async Task<DomainOfAggregate<Users>?> GetByIdentityAsync(string identityId, CancellationToken cancellationToken)
        {
            var entity = await context.Users
                .Include(x => x.Rentals)
                .FirstOrDefaultAsync(x => x.IdentityId == identityId, cancellationToken);

            if (entity == null)
            {
                return null;
            }

            return new UsersRentalsDomain(entity);
        }

        public async Task<DomainOfAggregate<Users>?> GetWithoutRentalsAsync(int aggregateId, CancellationToken cancellationToken)
        {
            var entity = await context.Users
                .FirstOrDefaultAsync(x => x.Id == aggregateId, cancellationToken);

            if (entity == null)
            {
                return null;
            }

            return new UsersRentalsDomain(entity);
        }

        public async Task<DomainOfAggregate<Users>?> GetByIdentityWithoutRentalsAsync(string identityId, CancellationToken cancellationToken)
        {
            var entity = await context.Users
                .FirstOrDefaultAsync(x => x.IdentityId == identityId, cancellationToken);

            if (entity == null)
            {
                return null;
            }

            return new UsersRentalsDomain(entity);
        }

        public Task SaveAsync(CancellationToken cancellationToken) => context.SaveChangesAsync(cancellationToken);
    }
}
