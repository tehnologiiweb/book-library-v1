﻿using BookLibrary.Core.DataModel;
using BookLibrary.Core.Domain.Book;
using BookLibrary.Core.SeedWork;
using Microsoft.EntityFrameworkCore;

namespace BookLibrary.Infrastructure.Data.Repositories
{
    public class BooksRepository : IBooksRepository
    {
        private readonly BookLibraryContext context;

        public BooksRepository(BookLibraryContext context)
        {
            this.context = context;
        }

        public async Task AddAsync(InsertBookInLibraryCommand command, CancellationToken cancellationToken)
        {
            var keywords = command.Keywords.Select(x => new Keywords(x.Name, x.Description)).ToList();

            var book = new Books(command.Name, command.Author, command.Genre, command.MaximumDaysForRental);
            book.Keywords = keywords;

            await context.Books.AddAsync(book, cancellationToken);
            await SaveAsync(cancellationToken);
        }

        public async Task<DomainOfAggregate<Books>?> GetAsync(int aggregateId, CancellationToken cancellationToken)
        {
            var book = await context.Books
                .FirstOrDefaultAsync(x => x.Id == aggregateId, cancellationToken);

            if (book == null)
            {
                return null;
            }

            return new BooksDomain(book);
        }

        public async Task DeleteBookAsync(int aggregateId, CancellationToken cancellationToken)
        {
            var book = await context.Books
                .FirstOrDefaultAsync(x => x.Id == aggregateId, cancellationToken);

            if (book != null)
            {
                context.Books.Remove(book);
            }
        }

        public Task SaveAsync(CancellationToken cancellationToken)
        {
            return context.SaveChangesAsync(cancellationToken);
        }
    }
}
