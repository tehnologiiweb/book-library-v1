﻿using BookLibrary.Core.DataModel;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace BookLibrary.Infrastructure.Data.EntityConfigurations
{
    internal class BooksConfiguration : EntityConfiguration<Books>
    {
        public override void Configure(EntityTypeBuilder<Books> builder)
        {
            builder
                .Property(x => x.Name)
                .IsRequired();

            builder
                .Property(x => x.Genre)
                .IsRequired();

            builder
                .Property(x => x.Author)
                .IsRequired();

            builder
                .OwnsMany(x => x.Keywords);

            base.Configure(builder);
        }
    }
}
