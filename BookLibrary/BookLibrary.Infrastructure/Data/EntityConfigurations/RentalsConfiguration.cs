﻿using BookLibrary.Core.DataModel;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace BookLibrary.Infrastructure.Data.EntityConfigurations
{
    internal class RentalsConfiguration : EntityConfiguration<Rentals>
    {
        public override void Configure(EntityTypeBuilder<Rentals> builder)
        {
            builder
                .Property(x => x.BookId)
                .IsRequired();

            builder
                .Property(x => x.ReaderId)
                .IsRequired();

            builder
                .Property(x => x.RentalStartDate)
                .IsRequired();

            builder
                .Property(x => x.ExpectedRentalEndDate)
                .IsRequired();

            builder
                .HasOne<Books>()
                .WithMany()
                .HasForeignKey(x => x.BookId)
                .OnDelete(DeleteBehavior.ClientSetNull);

            base.Configure(builder);
        }
    }
}
